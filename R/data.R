#' Common Primers for Microbial Community Analysis
#'
#' A dataframe containing sense and reverse complementary sequences of common primers
#'
#' \itemize{
#'   \item Name. The primer names
#'   \item Seq. The primer sequences
#'   \item Seq_rc. The reverse complementary primer sequences
#'   \item Note. Additional information, e.g. region amplified by the primer.
#' }
#'
#' @keywords datasets
#' @name mca_primers
#' @usage data(mca_primers)
#' @format A data frame with 5 rows and 4 columns
#' @source https://www.starseq.com/life-science/next-generation-sequencing/microbiome-analysis/
NULL

#' ZymoBIOMICS Microbial Community Standard: Theoretical 16S Proportions
#'
#' A dataframe containing the samples and theoretical proportions of the ZBMCS.
#'
#' \itemize{
#'   \item Genus. The genus names of the contained taxa
#'   \item Abundance. The portional abundance of the respective taxa
#' }
#'
#' @docType data
#' @keywords datasets
#' @name zbmcs_prop
#' @usage data(zbmcs_prop)
#' @format A data frame with 8 rows and 2 columns
#' @source https://www.zymoresearch.com/collections/zymobiomics-microbial-community-standards
NULL

#' ZymoBIOMICS Microbial Community Standard: FASTA Reference Sequences
#'
#' A Biostrings DNAStringSet containing the ZBMCS reference sequences.
#'
#' @docType data
#' @keywords datasets
#' @name zbmcs_fasta
#' @usage data(zbmcs_fasta)
#' @format A Biostrings::DNAStringSet with 49 sequences from 8 organisms
#' @source https://www.zymoresearch.com/collections/zymobiomics-microbial-community-standards
NULL
