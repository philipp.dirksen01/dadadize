#' Cutadapt Paried
#'
#' Cutadapt wrapper for paired end reads
#'
#' @param cutadapt_path The path to the cutadapt executable
#' @param dir_in The path to the directory containing the input sequences
#' @param dir_out The path of where to save the output
#' @param fp The forward sequencing primer
#' @param rp The reverse sequencing primer
#' @param fp_rc The reverse-complement forward sequencing primer
#' @param rp_rc The reverse-complement reverse sequencing primer
#'
#' @export
cutadapt_paired <- function(dir_in, dir_out,
                            fp, rp,
                            fp_rc, rp_rc,
                            cutadapt_path = "/opt/condaenvs/cutadapt/bin/cutadapt"
                            ) {
    if (dir.exists(dir_out)) {
        unlink(dir_out, recursive = TRUE)
    }
    dir.create(dir_out)

    fas_files1 <- list.files(dir_in, pattern = "_R1_001.fastq.gz")
    print(fas_files1)
    fas_files2 <- list.files(dir_in, pattern = "_R2_001.fastq.gz")
    print(fas_files2)

    for (f in seq_along(fas_files1)) {
        command <- paste0(cutadapt_path,
                         " -g ", fp, " -G ", fp_rc,
                         " -a ", rp, " -A ", rp_rc,
                         " -j 0 ", " -n 2 ", " -m 100 ",
                         " -o ", dir_out, "/%s ", " -p ", dir_out, "/%s ",
                         dir_in, "/%s ", dir_in, "/%s "
        )
        cmd <- sprintf(command,
                       fas_files1[f], fas_files2[f],
                       fas_files1[f], fas_files2[f])
        print(cmd)
        system(cmd)
    }
}
