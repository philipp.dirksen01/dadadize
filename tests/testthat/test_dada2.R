test_that("dada2 paired end pipeline", {
    fastq_path <- system.file("extdata/testdata/fastq", package = "dadadize")

    dd <- dada2_paired_end(fastq_path, truncLen = c(240, 160), multithread = 2) # CRAN requires 2 cores

    seqtab <- dd$seqtab
    expect_equal(dim(seqtab), c(2, 127))

    stats <- dd$stats
    expect_equal(stats[, "input"], c(F3D0 = 7793, Mock = 4779))
    expect_equal(stats[, "nonchim"], c(F3D0 = 6573, Mock = 4269))
    
    ret <- compare_zbmcs(seqtab, c("F3D0", "Mock"))
    expect_equal(ret$nASV_zbmcs, c(0, 5))

    tax <- dada2_assign_taxonomy(seqtab, db = "silva", multithread = 2)
    expect_true(all(c("Firmicutes", "Bacteroidota", "Proteobacteria") %in% tax[, "Phylum"]))   
    expect_true(all(c("Listeria", "Bacillus", "Staphylococcus") %in% tax[, "Genus"]))   
})

