
data_targets = data/mca_primers.rda \
			   data/zbmcs_fasta.rda \
			   data/zbmcs_prop.rda

.PHONY: all
all: $(data_targets)

$(data_targets): data/%.rda: data-raw/%.R
	cd data-raw; Rscript $(<F)

.PHONY: clean
clean:
	rm -rf data/*.rda

.PHONY: test
test:
	Rscript -e "devtools::test()"

.PHONY: install
install:
	Rscript -e "devtools::document()"
	Rscript -e "devtools::check()" || true
	Rscript -e "devtools::install(build_vignettes = TRUE, upgrade = 'never')"

.PHONY: view
view:
	Rscript -e "browseVignettes('dadadize')"
